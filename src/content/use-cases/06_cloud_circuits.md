---
title: "Cloud Circuits (cloud_vc)"
---

In order to configure a cloud circuit, you would first have to
consult the 
[`product_offerings_list`](https://docs.ix-api.net/latest/#operation/product_offerings_list)
endpoint and filter by `type=cloud_vc`. Because, there are multiple
cloud providers that have different workflows for provisioning,
this documentation separates two workflows

1. **Provider First Workflow**
2. **Exchange First Workflow**

Each **Product Offering** will tell you which workflow to use. For
example, provisioning an *AWS Direct Connect* circuit implies the
*exchange first* workflow while a *Microsoft Azure Express Route*
circuit implies the *provider first* workflow. The property
`service_provider_workflow` defines how you wouuld have
provision.

## Provider first workflow
{{<split>}}
{{<split-content>}}

This workflow assumes that relevant product parameters are
configured within the cloud service provider before the IX-API is
consulted. E.g: for *Azure*, you would first create a *service key*
in the Azure portal or Azure API which implies a bandwidth and
service location.

After this service has been created, you would need to select a
compatible product offering from the product offerings endpoint.
A compatible product offering has the same product parameters as
the ones specified with the cloud provider system.

Some exchanges provide filtering by `cloud_key`. Using this
filter, the
[`product_offerings_list`](https://docs.ix-api.net/latest/#operation/product_offerings_list)
endpoint returns compatible product offerings for those implied
product parameters:
`GET /product-offerings?cloud_key=<azure_service_key>`

The **Product Offering** object contains further information about
the services which can be consumed within the cloud providers
network. For example, the `provider_vlans` property explains if
the circuit combines multiple VLANs behind it. (see Azure Express
Route Peerings)

In this case, the `cloud_vlan` property used in the 
[`network_service_configs_create`](https://docs.ix-api.net/latest/#operation/network_service_configs_create)
operation contains information about the on-ramp side (the
exchange connection to the cloud provider). For example, a `null`
value would group all existing peerings and transport them over
the circuit unchanged while a specific value (referring to a
specific peering) would strip those tags before it transmits them
over the circuit.

The `diversity` property of the product offering object refers to
the amount of on-ramp connections, i.e. a value of `1` means there
is no built-in redundancy when creating a network services from
this product offering, while a value of `2` means there are two
paths from the exchange network to the cloud service provider. In
the case of Microsoft Azure, the diversity value is always 2
which means you would have to either select the primary or the
secondary path in the NSC creation.

The path to the cloud provider is selected by providing the
`handover` property when creating the network service using
[`network_services_create.`](https://docs.ix-api.net/latest/#operation/network_services_create)

You need to select from those product offerings and then use the
`cloud_key` in the request to create the network service.

After that, you need to go ahead and create actual point to point
configurations by creating network service configs of type
`cloud_vc` which point to that network service using the
[`network_service_configs_create`](https://docs.ix-api.net/latest/#operation/network_service_configs_create) operation.
Exchanges may
accept unique combinations of the `handover` and `cloud_vlan` value
pairs creating a total of diversity times possible `cloud_vlan`
value combinations.

We list here the steps to obtain all the required properties.

* The `type` for the NSC must match the type of the network
  service which in this case is `cloud_vc`.
* The `product_offering` ID from the previously selected
  product offering response.
* The `network_service` created earlier.

You will also need **Account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
`billing_account` could either be your or your sub-customer
account ID depending on the billing policy of the exchange.

Next you will need to specify on which connection the service
will reside. On how to obtain a **connection ID** please refer to
[Connection](#connection). The connection needs to be in the same
`handover_metro_area_network` specified in the product offering.

When creating an NSC will also require a vlan config. Please see
the [Service VLAN](#service-vlan) section on how to specify it.

Specific services require specific contacts to be assigned to
specific roles in that service context. Effectively being
responsible for certain aspects of the service lifecycle (E.g:
NOC contact supposedly reachable 24/7 for emergencies). You can
find the required role assignments for a NSC using 
[`network_services_read`](https://docs.ix-api.net/latest/#operation/network_services_read)
on the selected **Network Service** in the
`nsc_required_contact_roles` property.

Once you found out the required role assignment you need to
either pick from existing or create the corresponding role
assignments and specify their **role assignments IDs** during the
creation of the NSC.

{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="network_service_create"
    example="cloud_vc_azure_create"
    method="POST"
    status="201"
    endpoint="/network-services">}}
{{</api-example>}}

{{<api-example
    operation="network_service_configs_create"
    example="cloud_vc_nsc_azure_create_1"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}

{{<api-example
    operation="network_service_configs_create"
    example="cloud_vc_nsc_azure_create_2"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}


{{</split-aside>}}
{{</split>}}


## Exchange first workflow

This workflow does not require relevant product parameters to be
configured within the cloud service provider before the IX-API is
used to create a Cloud Service, for example, for AWS, you just
need to have the AWS Account ID.

After you have the credential that identifies you at the Cloud
Service Provider, you would need to select a product offering
from the same cloud provider in the product offerings endpoint.

After you select the suitable product offering you can create the
cloud connect service following the same steps listed in 
the [**Provider First Workflow**](#provider-first-workflow)
after the **Product Offering** is selected.

This property is retrieved from the Cloud Service Provider
console and the table here provides details of where to retrieve
from:


* **Alibaba:** Alibaba Cloud Account ID
  
  **Example:**  `9876543210123456`

* **Amazon Web Services:** AWS Account ID

  **Example:** `123456789876`

* **Google:** Partner Paring Key

  This is obtained by first 
  [creating a VLAN attachment for a Partner Interconnect,](https://cloud.google.com/network-connectivity/docs/interconnect/how-to/partner/creating-vlan-attachments)
  ensuring to select the correct location.

  **Example:** `d8412846-c12b-3364-cd67-1471187ce9c9/europe-west3/2`

* **Microsoft Azure:** ExpressRoute Service Key

  This is obtained by first
  [creating an ExpressRoute Circuit,](https://docs.microsoft.com/en-us/azure/expressroute/expressroute-howto-circuit-portal-resource-manager#create)
  ensuring to select Interxion as the connectivity provider and
  the correct region.

  **Example**: `dc79e015-f15c-4681-9c7b-6d16b2973997`

* **Oracle:** FastConnect Virtual Circuit OCID

  This is obtained by first
  [creating a Virtual Circuit via a FastConnect Partner (Task
  4)](https://docs.oracle.com/en-us/iaas/Content/Network/Concepts/fastconnectprovider.htm)
  , ensuring to select Interxion as the FastConnect Partner and the correct location.

  **Example:** `ocid1.virtualcircuit.oc1.iad.aaaaaaaakskl9riko5fbhw822tnjo7c4un1b2...`

