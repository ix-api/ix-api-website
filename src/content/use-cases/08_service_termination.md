---
title: "Service Cancellation"
---

{{<split>}}
{{<split-content>}}
Network services as well as network service configs can be
terminated. While those objects are often bound to service
contracts, they may have certain cancellation policies. There is
a dedicated endpoint for network services as well as network
service configs which can be queried to answer the questions:

1. When can the service technically be decommissioned
2. When does my obligation to pay for it ends

For network service configs you can retrieve the cancellation
policy using the
[`network_service_config_cancellation_policy_read`](https://docs.ix-api.net/latest/#operation/network_service_config_cancellation_policy_read)
and for network services the
[`network_service_cancellation_policy_read`](https://docs.ix-api.net/latest/#operation/network_service_cancellation_policy_read) operation.

You may provide a *request parameter* for the earliest
decommissioning date: `GET /network-service-configs/<id>/cancellation-policy?decommission_at=YYYY-MM-DD`. 
The response then contains a `decommission_at`
property which can be passed in the request body to the deletion
of network services and network service configs.
{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="network_service_cancellation_policy_read"
    method="GET"
    status="200"
    endpoint="/network-services/<id>/cancellation-policy">}}
{{</api-example>}}
{{</split-aside>}}
{{</split>}}

{{<split>}}
{{<split-content>}}
To request the cancellation, use 
[`network_services_destroy`](https://docs.ix-api.net/latest/#operation/network_services_destroy)
for **Network Services** and for **Network Service Configs** use
[`network_service_configs_destroy`](https://docs.ix-api.net/latest/#operation/network_service_configs_destroy)

If you omit the `decommission_at` request parameter, the earliest possible date
will be assumed.

For all **Network Service Configs** associated with a **Network Service**
which is about to be decommissioned, the *deletion cascades*.

Furthermore, the deletion of **Network Service Configs**
*cascades to*
**Network Feature Configs** and **Member Joining Rules**.

{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="network_service_config_destroy"
    method="GET"
    status="200"
    endpoint="/network-services/<id>/cancellation-policy">}}
{{</api-example>}}
{{</split-aside>}}
{{</split>}}
