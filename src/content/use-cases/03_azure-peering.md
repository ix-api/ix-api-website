---
title: "Microsoft Azure Peering Service (MAPS)"
---

{{<split>}}
{{<split-content>}}

First you will need to choose the right **MAPS Product Offering**
which includes properties such as specific LAN instance and
bandwidth. Use the filters to find a product offering that
matches.

The `resource_type` of the product offering should be
`network_service_config`.

You now will need to find the MAPS network service to configure
by querying the **Network Services** endpoint and filter by the
ID of the product offering using [`network_services_list`:](https://docs.ix-api.net/latest/#operation/network_services_list) `GET /network-services?product_offering=<id>`

Next you will need to create a **Network Service Config (NSC)** in
order to join the MAPS virtual circuit. You will require several
properties as specified in the 
[`network_service_configs_create`](https://docs.ix-api.net/latest/#operation/network_service_configs_create) operation.

We list here the steps to obtain all the required properties and
constraints.

* The `type` for the NSC must match the type of the **Network
  Service.**
* The `product_offering` **ID** is the same as the previously
  selected **Product Offering** response.
* The `network_service` **ID** is retrieved from the
  `network_service_list` query.

You will also need **Account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
`billing_account` could either be your or your sub-customer
account ID depending on the billing policy of the exchange.

Next you will need to specify on which connection the service
will reside. On how to obtain a `connection` **ID** please refer
to [Connection](#connection) in Getting Started. The connection
needs to be in the same `handover_metro_area_network` specified
in the product offering.

When creating an NSC will also require a vlan config. Please see
the [Service VLAN](#service-vlan) section on how to specify it.

This service may require specific contacts to be assigned to
specific roles in that service context. You can find the required
role assignments for a NSC in the `nsc_required_contact_roles`
property in the Network Service you queried before.

After creating the **Network Service Config**, you need to
set up a [Route Server Session.](#route-server-sessions)


{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="network_service_configs_create"
    example="maps_nsc_create"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}
{{</split-aside>}}
{{</split>}}
