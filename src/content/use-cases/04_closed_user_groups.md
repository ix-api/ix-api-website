---
title: "Closed User Groups (mp2mp_vc)"
---

A closed user group is a participant-owned multi-user LAN
service. You could either join an existing closed user group or
create a new one.

## Joining a Closed User Group

{{<split>}}
{{<split-content>}}


Query the 
[`network_services_list`](https://docs.ix-api.net/latest/#operation/network_services_list)
endpoint to retrieve the list of
**Network Services**. You can filter by `type` in order to
only list closed user groups:
`GET /network-services?type=mp2mp_vc`

Within the result set, choose the appropriate network service.
Normally, the closed used groups identify themselves via the
property `display_name`.

The selected **Network Service** contains the property
`nsc_product_offerings` which is a list of compatible 
**Product Offerings** of `resource_type:` `network_service_config`.
You can use those ids to filter the 
[`product_offerings_list`](https://docs.ix-api.net/latest/#operation/product_offerings_list)
endpoint: `GET /product-offerings?id=id1,id2,id3...`

Once, you found a suitable product_offering (i.e. by choosing
`bandwidth` or `handover_metro_area_network`) you can go ahead and
create the **Network Service Config**. For this, use 
[`network_service_configs_create`](https://docs.ix-api.net/latest/#operation/network_service_configs_create)
with the `network_service` and selected `product_offering` IDs.

Some exchanges require or allow `mp2mp_vc` circuits with **MAC
ACL Protection**.  In those cases, you would find a value
describing the policy in the **Network Service** object in
property `mac_acl_protection`.

For the selection and adding a **MAC Address**, please see
configuring [Exchange LAN Public Peering.](#exchange-lan-public-peering)

We list here the steps to obtain all the required properties.

* The `type` for the NSC must match the type of the network
  service which in this case is `mp2mp_vc`.
* The `product_offering` **ID** from the previously selected
  product offering response.
* The `network_service` **ID** selected earlier.

You will also need **Account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
`billing_account` could either be your or your sub-customer
account ID depending on the billing policy of the exchange.

Next you will need to specify on which connection the service
will reside. On how to obtain a **connection ID** please refer to
[Connection](#connection). The connection needs to be in the same
`handover_metro_area_network` specified in the product offering.

When creating an NSC will also require a vlan config. Please see
the [Service VLAN](#service-vlan) section on how to specify it.

Specific services require specific contacts to be assigned to
specific roles in that service context. Effectively being
responsible for certain aspects of the service lifecycle (E.g:
NOC contact supposedly reachable 24/7 for emergencies). You can
find the required role assignments for a NSC using 
[`network_services_read`](https://docs.ix-api.net/latest/#operation/network_services_read)
on the selected **Network Service** in the
`nsc_required_contact_roles` property.

Once you found out the required role assignment you need to
either pick from existing or create the corresponding role
assignments and specify their **role assignments IDs** during the
creation of the NSC.

{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="network_service_configs_create"
    example="mp2mp_nsc_create"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}
{{</split-aside>}}
{{</split>}}

## Creating a Closed User Group

{{<split>}}
{{<split-content>}}
First select a product offering of the type `mp2mp_vc` matching
your criteria like handover and service metro area network.

Then create the network service using
[`network_services_create`](https://docs.ix-api.net/latest/#operation/network_services_create)
with `type` `mp2mp_vc` and providing the **ID** of the
selected `product_offering`.

You will also need **Account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
`billing_account` could either be your or your sub-customer account
ID depending on the billing policy of the exchange. Optionally
you could specify a display name for the network service to make
it easily identifiable by other users.

Closed user groups can have an either **public** or **private** joining
policy.

If private, the network service is only visible to users that
have been invited by the owner by creation of a specific member
joining rule for the joining member.

If public, the network service is visible to all users of the API
and able to join unless there is a member joining rule in place,
denying such access.

Creat a member joining rules with the
[`member_joining_rules_create`](https://docs.ix-api.net/latest/#operation/member_joining_rules_create)
operation.

You can choose to either `allow` or `deny` access to a customer
identified by the `consuming_account` property. You need to
specify the id of the `network_service`.

In case of allow, you can specify a minimal bandwidth requirement
or limit.

Some exchanges might require you to join your own `mp2mp_vc`
**Network Service** in order for it to become operational. In this
case, follow the instructions in the above section.
{{</split-content>}}

{{<split-aside>}}
{{<api-example
    operation="network_services_create"
    example="mp2mp_ns_create"
    method="POST"
    status="201"
    endpoint="/network-services">}}
{{</api-example>}}

{{<api-example
    operation="member_joining_rules_create"
    expand="true"
    method="POST"
    status="201"
    endpoint="/member-joining-rules">}}
{{</api-example>}}

{{</split-aside>}}
{{</split>}}

