---
title: "Private Virtual Circuit (p2p_vc)"
---

A **Private Virtual Circuit** (sometimes referred to also as private
VLAN) is a participant-owned LAN service between two parties. You
could either join an existing private virtual circuit upon
invitation by the owner or create a new one.

## Creating a Private Virtual Circuit

{{<split>}}
{{<split-content>}}
First select a product offering of the type `p2p_vc` matching
your criteria like *handover* and *service metro area network*.

Then create the **Network Service** using with the
[`network_services_create`](https://docs.ix-api.net/latest/#operation/network_services_create)
operation.

Choosing the type: `p2p_vc` and providing the id of the selected
product offering.

You will also need **Account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
billing_account could either be your or your sub-customer account
ID depending on the billing policy of the exchange.

Furthermore private virtual circuits require you to specify a
`joining_member_account`. That is the account ID of the B side
party of the service. The account ID for the B side has to be
communicated directly by the user, alternatively if the B side
user specified in account property _discoverable_ as true such ID
would be queryable using 
[`accounts_list`.](https://docs.ix-api.net/latest/#operation/accounts_list)

Finally if you want to create a private virtual circuit between
two of your own connections you can specify your own account ID
as `joining_member_account`.

Optionally you could specify a `display_name` for the network
service to make it easily identifiable by other users.

Once the **Network Service** is created then you can proceed
creating the respective **A-Side Network Service Config**.

The created **Network Service** has the property
`nsc_product_offerings` which is a list of compatible
`resource_type` **Network Service Config Product Offerings**. If
the property contains an empty list that means that the NSC does
not have a commercial impact and can be created without the
product offering property.  If not empty, you can use those ids
to filter the 
[`product_offerings_list`](https://docs.ix-api.net/latest/#operation/product_offerings_list)
endpoint like this `GET /product-offerings?id=id1,id2,id3...`

Once, you found a suitable `product_offering` (i.e. by choosing
bandwidth or handover metro area network) you can go ahead and
create the **Network Service Config**. For this, use
[`network_service_configs_create`](https://docs.ix-api.net/latest/#operation/network_service_configs_create)
with the `network_service` id and `product_offering` which can be
found here. 

We list here the steps to obtain all the required properties.

* The `type` for the NSC must match the type of the network
  service which in this case is `p2p_vc`.
* The `product_offering` ID from the previously selected
  product offering response.
* The `network_service` ID selected earlier.

You will also need **Account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
`billing_account` could either be your or your sub-customer
account ID depending on the billing policy of the exchange.

Next you will need to specify on which connection the service
will reside. On how to obtain a **connection ID** please refer to
[Connection](#connection). The connection needs to be in the same
`handover_metro_area_network` specified in the product offering.

When creating an NSC will also require a vlan config. Please see
the [Service VLAN](#service-vlan) section on how to specify it.

Specific services require specific contacts to be assigned to
specific roles in that service context. Effectively being
responsible for certain aspects of the service lifecycle (E.g:
NOC contact supposedly reachable 24/7 for emergencies). You can
find the required role assignments for a NSC using the 
[`network_services_read`](https://docs.ix-api.net/latest/#operation/network_services_read)
operation under `nsc_required_contact_roles`.

Once you found out the required role assignment you need to
either pick from existing or create the corresponding role
assignments and specify their **role assignments IDs** during the
creation of the NSC.

{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="network_services_create"
    example="p2p_ns_create"
    method="POST"
    status="201"
    endpoint="/network-services">}}
{{</api-example>}}

{{<api-example
    expand="true"
    operation="network_service_configs_create"
    example="p2p_nsc_a_create"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}


{{</split-aside>}}
{{</split>}}

## Configuring the B side

{{<split>}}
{{<split-content>}}

In order to configure the **B-Side** you will need to know the
**Network Service ID** for the private virtual circuit.

Unless you received the ID directly from the inviting party, you
can use the
[`network_services_list`](https://docs.ix-api.net/latest/#operation/network_services_list)
operation to retrieve the list of already existing network
services and filter by `type=p2p_vc` in order to only list **Private
Virtual Circuits**.  Within the result set, choose the appropriate
object.  Normally, the private virtual circuits identify
themselves via the property `display_name`.

Once the network service has been identified the **B-Side** **Network
Service Config** can be created following the same instruction as
the **A-Side**, as specified in the above section.

Both NSCs need to be compatible with the NS specifications and
only when the three entities' creation has been performed can
they be provisioned.

{{</split-content>}}
{{<split-aside>}}

{{<api-example
    expand="true"
    operation="network_service_configs_create"
    example="p2p_nsc_b_create"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}

{{</split-aside>}}
{{</split>}}

