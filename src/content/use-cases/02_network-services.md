---
title: "Exchange LAN Public Peering"
---

{{<split>}}
{{<split-content>}}

As mentioned above, you will need to find the suitable public
peering service product offering, which would include properties
such as specific LAN instance and bandwidth. The list of product
offerings can be obtained using the 
[`product_offerings_list`](https://docs.ix-api.net/latest/#operation/product_offerings_list)
operation with an applied filter: `GET /product-offerings?type=exchange_lan`

The complete list of *required properties* is specified in the
response of the product [`product_offerings_read`](https://docs.ix-api.net/latest/#operation/product_offerings_read) endpoint.

Next you will need to create a **Network Service Config (NSC)** which
will represent your public peering service. In order to do that,
you will require several properties as described in the
[`network_service_configs_create`](https://docs.ix-api.net/latest/#operation/network_service_configs_create) operation.

We list here the steps to obtain all the required properties.

The `type` for the NSC must match the type of the network
service which in this case is _exchange_lan_.

The `product_offering` ID is the same as the previously
selected product offering response.

Likewise, the `network_service` ID is available in the
previously selected **product offering** response under
`exchange_lan_network_service`.

You will also need **account IDs** (`managing_account`,
`consuming_account`, `billing_account`). If you are creating the
service for yourself they will all match. Otherwise the
`consuming_account` will be your sub-customer account ID and the
`billing_account` could either be your or your sub-customer
account ID depending on the billing policy of the exchange.

Next you will need to specify on which connection the service
will reside. On how to obtain a **connection ID** please refer to
[Connection.](#connection)

The connection needs to be in the same
`handover_metro_area_network` specified in the product offering.

When creating an NSC will also require a vlan config. Please see
the [Service VLAN](#service-vlan) section on how to specify it.

Specific services require specific contacts to be assigned to
specific roles in that service context. Effectively being
responsible for certain aspects of the service lifecycle (E.g:
NOC contact supposedly reachable 24/7 for emergencies). You can
find the required role assignments for a NSC in the corresponding
[network_services_read](https://docs.ix-api.net/latest/#operation/network_services_read)
response in the `nsc_required_contact_roles` property.

Once you found out the required role assignment you need to
either pick from existing or 
[create the corresponding role assignments](#contact)
and specify their **role assignments IDs** during the
creation of the NSC.

Finally, for public peering services, you will also require a MAC
address. You can list mac addresses using
[`macs_list`](https://docs.ix-api.net/latest/#operation/macs_list)
or register a new mac address via 
[`macs_create`.](https://docs.ix-api.net/latest/#operation/macs_create)

Once you have selected a suitable MAC address you can use the
**MAC address ID** during the creation of the NSC.

{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="macs_create"
    method="POST"
    status="201"
    endpoint="/macs">}}
{{</api-example>}}

{{<api-example
    operation="network_service_configs_create"
    example="exchange_lan_nsc_create"
    method="POST"
    status="201"
    endpoint="/network-service-configs">}}
{{</api-example>}}
{{</split-aside>}}
{{</split>}}

{{<split>}}
{{<split-content>}}
Some exchanges might require you to create a **Network Feature
Config (NFC)** alongside the NSC.

You can find the required Network Features to configure for
a specific NSC with the 
[`network_features_list`](https://docs.ix-api.net/latest/#operation/network_features_list)
operation and providing the ID of the *network service* and *required* as
filters: `GET /network-features?required=true&network_service=<network_service_id>`


If an NFC is required the corresponding NSC will stay in an `error`
state until the NFC has been created. On how to create a **NFC**
please refer to [setting up a Route Server
Session](#route-server-sessions).

Depending on each exchange level of automation the provisioning
of the service may or may not be on demand. Because of that you
may need to poll the state of the newly created service via this
using 
[`network_service_configs_read`](https://docs.ix-api.net/latest/#operation/network_service_configs_read)

{{</split-content>}}
{{<split-aside>}}

{{<api-example
    operation="network_features_list"
    method="GET"
    status="200"
    endpoint="/network-features">}}
{{</api-example>}}

{{</split-aside>}}

{{</split>}}

### Optional: Update peering IP FQDN

{{<split>}}
{{<split-content>}}
You may want to update the peering IP FQDN, in order to do so you
will need to find the IP address the exchange assigned for your
public peering service, using the 
[`ips_list`](https://docs.ix-api.net/latest/#operation/ips_list)
operation, filtering by network service config ID:
`GET /ips?network_service_config=<nsc_id>`

And then you will need to update the FQDN property with
[`ips_partial_update`](https://docs.ix-api.net/latest/#operation/ips_partial_update).
{{</split-content>}}
{{<split-aside>}}
{{<api-example
    expand="true"
    operation="ips_partial_update"
    method="PATCH"
    status="200"
    endpoint="/ips/<id>">}}
{{</api-example>}}
{{</split-aside>}}
{{</split>}}

