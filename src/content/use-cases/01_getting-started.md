---
title: "Getting Started"
---

## Account


{{<split>}}

{{<split-content>}}
In order to use IX-API, you need to have an account with the
Internet Exchange. This can either be your own existing account
(created by the IXP), or if you order on behalf of a
sub-customer, you can create it yourself, using the
[`accounts_create`](https://docs.ix-api.net/latest/#operation/accounts_create)
operation.

Fill in the details of your sub customer. If you are a reseller,
the "`billing_information`" should point to your own invoicing
details.

The "`managing_account`" must point to your own (reseller) account
ID. In order to find your own account ID, please use the 
[`accounts_list`](https://docs.ix-api.net/latest/#operation/accounts_list)
operation.

The account that does not have a "`managing_account`" is your
account.

After creation, a GET request will retrieve the status of the
contract creation request. The "`state`" and "`status`" attributes
give an indication of next steps. This could involve an
out-of-band membership process, document signing, the creation of
contacts/roles, or the order of a network service to activate a
membership.

The "`state`" attribute is a single word indicating the state of
the account, e.g. "`requested`", "`production`", "`error`" (see
[States Documentation.](https://docs.ix-api.net/latest/#section/State))

The "`status`" is a more detailed, human-readable description of
the account state, including instructions on how to move from
e.g. "`requested`" or "`error`" to "`production`".

{{</split-content>}}
{{<split-aside>}}
{{<api-example
    operation="accounts_create"
    method="POST"
    status="201"
    endpoint="/accounts">}}
{{</api-example>}}
{{</split-aside>}}

{{</split>}}

## Contact

{{<split>}}
{{<split-content>}}
In order for an account to be operational or Terms & Conditions
to be accepted, some exchanges may require contacts assigned to
specific roles.  The exchanges should provide such information in
the account status field mentioned above.

You can create a contact with the
[`contacts_create`](https://docs.ix-api.net/latest/#operation/contacts_create)
operation.

The role assignment can then be performed finding the requested
role using
[`roles_list`.](https://docs.ix-api.net/latest/#operation/roles_list)

Finally assigning the requested role to the created or
pre-existing contact, with the 
[`role_assignments_create`](https://docs.ix-api.net/latest/#operation/role_assignments_create)
operation.
{{</split-content>}}

{{<split-aside>}}
{{<api-example
    expand="true"
    operation="contacts_create"
    method="POST"
    status="201"
    endpoint="/contacts">}}
{{</api-example>}}

{{<api-example
    expand="true"
    operation="roles_list"
    method="GET"
    status="200"
    endpoint="/roles">}}
{{</api-example>}}

{{<api-example
    expand="true"
    operation="role_assignments_create"
    method="POST"
    status="201"
    endpoint="/role-assignments">}}
{{</api-example>}}


{{</split-aside>}}

{{</split>}}


## Connection

{{<split>}}
{{<split-content>}}
Some exchanges might support the provisioning of connections via
IX-API, some will require you to order them using a different
interface and process. For the first case, please refer to
ordering a connection section . Regardless, any new network
service will require a connection to be deployed onto, and
therefore this constitutes a prerequisite to the follow up
documentation.

Your connection list can be retrieved with the 
[`connections_list`](https://docs.ix-api.net/latest/#operation/connections_list)
operation.
{{</split-content>}}

{{<split-aside>}}
{{<api-example
    operation="connections_list"
    method="GET"
    status="200"
    endpoint="/connections">}}
{{</api-example>}}
{{</split-aside>}}

{{</split>}}

## Service VLAN

Each creation of a *Network Service Config* requires a VLAN config
to be specified.

The VLAN types and ethertypes offered by an Exchange can be
discovered from the "`vlan_types`" and "`outer_vlan_ethertypes`"
properties for each individual [Connection](#connection). These may vary within
an Exchange depending on constraints within connected hardware.

Exchanges may choose to support multiple types of VLAN tagging.
Currently the possible types are:

1. **Dot1Q**: Providing a *single VLAN* tag to identify the service.
2. **QinQ**: Providing *two VLAN tags* to identify the service.
3. **Port**: Untagged.

The supported ethertype of the outer tag in hexadecimal notation
are: "`0x8100`" (standard 802.1q), "`0x88a8`" (8021.ad stacked tags),
and "`0x9100`" (pre-802.1q Cisco VLAN tags)


## Product Offerings

{{<split>}}
{{<split-content>}}
IX-API v2 supports a variety of network services from standard
peering to various private interconnection options. In order to
discover them you can list the product offerings with
[`product_offerings_list`.](https://docs.ix-api.net/latest/#operation/product_offerings_list)

Once found the suitable offering, its ID can be used to order the
service itself. As mentioned above, some exchanges may require
specific services to be ordered first for new accounts to move to
production status. In that case the related info will be
available in the account "`status`" field.

We will provide a guide for each different service provisioning
starting from peering.
{{</split-content>}}
{{<split-aside>}}
{{<api-example
    operation="product_offerings_list"
    method="GET"
    status="200"
    endpoint="/product-offerings">}}
{{</api-example>}}
{{</split-aside>}}

{{</split>}}

