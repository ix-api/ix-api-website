---
title: "Route Server Sessions"
---

Some network services require you to set up a session with a
route server. Route Servers are considered network features and
you will have to create a network feature config for each feature
marked as required.

## Network Features

{{<split>}}
{{<split-content>}}
Network features are additional functionalities to network
services. Currently, there is only a single type of network
features, which is route_server. Those route server network
features only make sense in the context of peering 
(i.e. [Public Peering](#exchange-lan-public-peering)
or
[MAPS](#microsoft-azure-peering-service-maps)).

You can find all existing network features with the
[`network_features_list`](https://docs.ix-api.net/latest/#operation/network_features_list)
operation.

Network features have `flags` which represent configuration
options. For example, an exchange may choose to enable the client
to configure *RPKI filtering* on the BGP session. In this case, the
network feature returns a flag called RPKI with an appropriate
description. Some of those flags are always enabled. As such,
their mandatory property is set to true which means they may
never be disabled.  The purpose of those mandatory flags is
purely for documentation.
{{</split-content>}}
{{<split-aside>}}

{{<api-example
    operation="network_features_list"
    method="GET"
    status="200"
    endpoint="/network-features">}}
{{</api-example>}}

{{</split-aside>}}
{{</split>}}

## Network Feature Configs

{{<split>}}
{{<split-content>}}
Network features describe route server instances while network
feature configs describe sessions on those instances. In order to
create a route-server session, you would first need to have a
fully provisioned network service config which means it is in
state production (or in an error state if the NFC is required for
that NSC). It is safe to assume that this network service config
contains ip addresses that can be used in the peering context and
that corresponding network service refers to a list of network
features. You can create the NFC with
[`network_feature_configs_create`.](https://docs.ix-api.net/latest/#operation/network_feature_configs_create)

There can be one BGP session between each pair of NSC IP
addresses and network features. You have to choose the IP address
from the list of IPs of the NSC and specify it as part of the NFC
creation.

{{</split-content>}}
{{<split-aside>}}

{{<api-example
    operation="network_feature_configs_create"
    method="POST"
    status="201"
    endpoint="/network-feature-configs">}}
{{</api-example>}}

{{</split-aside>}}
{{</split>}}
