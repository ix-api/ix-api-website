---
title: For the ‘Good of the Internet’ 
date: 2019-11-17
---

**Global Internet Exchange Leaders Develop Universal API**

The world’s leading Internet Exchange operators
[AMS-IX](https://www.ams-ix.net/) (Amsterdam),
[DE-CIX](https://www.de-cix.net/) (Frankfurt) and
[LINX](https://www.linx.net/) (London) have joined forces to develop
a common Application Programming Interface (API) to provision and
configure interconnection services. This so-called IX-API will
improve productivity for their members, customers and partners
alike. It allows users to self-manage their existing and new
Interconnection services such as peering more effectively, from
ordering new ports to monitoring provisioning processes.

The consortium of the three Internet Exchanges also called on common
member and connectivity partner Epsilon
([www.epsilontel.com](https://www.epsilontel.com/)) and data centre
partner Interxion ([www.interxion.com](https://www.interxion.com/))
to act as pilot customers for the new software. Both partners’ tests
came out with positive results and they will be joining the
exchanges at the European Peering Forum (EPF) taking place in
September to present and demonstrate the tool to the rest of the
Internet and telecommunications community.

Dr Thomas King, Chief Technology Officer at DE-CIX says; “DE-CIX
ambition has always been to make customers life easier and connect
them anywhere needed on the planet. Therefore, we are pleased to
have come together with our partners to create this IX-API from
scratch. We are convinced that this new industry standard will be
adopted by other Internet Exchanges and interconnection providers.”

Richard Petrie, Chief Technology Officer for LINX adds; “Our
software and engineering teams have collaborated, working hard to
create this modern RESTful API based on OpenAPI Specification v3 for
managing IXPs customers and services.”

Henk Steenman, Chief Technology Officer for AMS-IX says; “At AMS-IX,
we are very proud that the three largest exchanges join forces,
collaborate and have reached consensus to deliver the common IX-API
project. In a very competitive market, we have found a common
ambition and drive for the greater good of the internet.”

There is a large percentage of member and customer crossover between
all three exchanges, working with the same remote peering partners
and data centres. Being able to work with and offer out to the
community, a universal API tool is a huge step forward for the
industry.

AMS-IX, DE-CIX and LINX are part of the European Peering Forum (EPF)
and aim to be unveiling the new project, in its completion, to their
industry peers at EPF14 this September in Tallinn, Estonia.

