---
title: "Licensing"
---

IX-API is licensed under [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0) 
(open and liberal license). If you do changes that break the compatibility,
please inform 
[the participating IXs](https://ix-api.net/about-ix-api/participating-ixs/) 
and, of course, your customers.

