---
title: "Board and Governance Model"
layout: text
---

The three founding members of the IX-API AMS-IX, DE-CIX and LINX have one seat
in the IX-API board each. The board reviews the recommendations and the
feedback from IX customers or participating IXs (members) and decides on how to
develop and evolve the IX-API. Decisions are made on mutual agreement.

Beginning in Summer 2019, the IX-API Board is defined as stated above. Every
two years, an election will be organized, and the Board can be extended in the
future if needed.

You can submit feedback or proposals on how to enhance the IX-API specification
via the [gitlab issue tracker](https://gitlab.com/ix-api/ix-api-schema/-/issues)
at any time.
The better and the more detailed the description of the proposal is, the easier it
is to select it.
