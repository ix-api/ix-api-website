
## Developed for the community

IX-API was designed and developed as an open industry standard by AMS-IX,
DE-CIX and LINX, the three leading global Internet Exchanges. The open
community API (Application Programming Interface) is already implemented at
these three IXs, and additional Internet Exchanges are in the process of
implementing it.

All customers of the participating exchanges, whether
resellers, Internet Service Providers, Content Delivery Networks or carriers,
can benefit from IX-API, regardless of their business model and size.


{{<linkBtn href="/about-ix-api">}}About IX-API{{</linkBtn>}}
