---
hidden: true
---

This section provides content for the front page.
The content is included in `layouts/index.html`.
