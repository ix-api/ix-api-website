
# Simplify your IX services

IX-API provides an interface for provisioning key services at
multiple Internet Exchanges (IXs). It supports fully end-to-end
automated processes and enables networks to configure,
change and cancel services at multiple IXs.

## Explore the specs and get the sandbox
The best way to check out how you can use the IX-API is to explore and test the API yourself.

Our “Getting started” section shows you the IX-API specs and how to get the sandbox. Or you jump directly to the API Documentation.

{{<linkBtn href="/api-documentation/getting-started">}}Getting Started{{</linkBtn>}}

{{<linkBtn target="_blank" href="https://docs.ix-api.net/latest">}}Reference Documentation{{</linkBtn>}}

## Why you should test it

With IX-API, you are able to easily configure, change and cancel IX services at
multiple IXs. The benefits are manifold, both for Internet Exchanges offering
the API and receiving customer requests as well as for IX customers configuring
and changing their IX services.

Read more about usecases and workflows to
simplify your IX services.

