
## Automated processes, cost-efficient and fast

IX-API leads to a variety of benefits for IX customers
and members as well as for IXs. It reduces the time for
configuring, changing and cancelling services from days to minutes.  IX-API is
available 24/7/365 and it requires less effort and cost per transaction. IX-API
offers increased transparency and by offering one single API language for
multiple Internet Exchanges, implementation costs are reduced to a minimum.

IX-API provides an interface for provisioning key services at multiple Internet
Exchanges (IXs). It supports fully end-to-end automated processes and enables
networks to configure, change and cancel services at multiple IXs.

{{<linkBtn href="/about-ix-api/benefits/">}}Benefits{{</linkBtn>}}
