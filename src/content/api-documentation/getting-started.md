---
title: "Getting Started"
date: 2021-04-19T18:32:24+02:00
---

## Explore the Specs
IX-API is a REST API following [OpenAPI v3](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md) standards. We provide a "Swagger UI"
style interface (powered by ReDoc) for easy exploration 
[here](https://docs.ix-api.net/).

The examples in the documentation will give you a clear picture of the
endpoints and what data they provide or expect.  Also, the glossary gives you
an overview and the IX-API definitions.

Here are some starting points for
exploring:

* [List facilities](https://docs.ix-api.net/v2/#operation/facilities_list)
* [List your connections (LAGs)](https://docs.ix-api.net/v2/#operation/connections_list)
* [Get details about a connection](https://docs.ix-api.net/v2/#operation/connections_read)
* [Add a MAC address](https://docs.ix-api.net/v2/#operation/macs_create)
* [Get details about your network service configuration](https://docs.ix-api.net/v2/#tag/network-service-configs)


## Get the sandbox 
We provide a Docker image to facilitate local exploration and adoption of the
API. The sandbox allows you to create tokens for (local) authentication. It
comes with a skeleton config for a quick start.

You can then import the IX-API specifications into Postman or a similar tool
and let it point at your local installation.

Follow these steps:

* Make sure you have got Docker and Postman installed.
* Follow the steps in the README (part of the sandbox repository) using the
  public image at Dockerhub.
* Go to http://localhost:8000/ and check whether the sandbox is running.
* Use the bootstrapping scripts to give you a quick start on a demo
  environment.
* Also the README gives you in-depth information about the sandbox for IX-API.

## Questions? 

If you have any questions, please do not hesitate to contact us.
