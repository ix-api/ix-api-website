
/**
 * Toggle class on an element
 */
const toggleClass = (el, className) => {
  const cls = el.classList;
  if (cls.contains(className)) {
    cls.remove(className);
  } else {
    cls.add(className);
  }
};

/**
 * API examples event listeners
 */
const apiExamplesInit = () => {
  // Toggle expand button
  const expand = document.querySelectorAll(".re-body-expand button");
  const toggleOpen = (e) => {
    e.preventDefault();
    const body = e.target.parentElement.parentElement;
    toggleClass(body, 'expanded');
  };

  // Bind event listeners
  expand.forEach((el) => el.addEventListener("click", toggleOpen));
  expand.forEach((el) => el.addEventListener("touchstart", toggleOpen));
};

/**
 * Initialize menu, this ist mostly for the mobile menu.
 */
const menuInit = () => {
  // Get menu element for toggleing visibility classes
  const navMain = document.querySelector('.main-nav');
  const onClickOpen = (e) => {
    e.preventDefault();
    toggleClass(navMain, 'main-nav--open');
  };

  // Get hamburger button element and bind touch event
  const menuAccessor = document.querySelector('.menu-accessor');
  menuAccessor.addEventListener('touchstart', onClickOpen);
  menuAccessor.addEventListener('click', onClickOpen);
};

document.addEventListener('DOMContentLoaded', () => {
  menuInit();
  apiExamplesInit();
});

